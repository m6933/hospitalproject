from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import sqlite3

#  command functions
def register() :
    matricule = entryMatricule.get()
    nom = entryNom.get()
    prenom = entryPrenom.get()
    age = entryAge.get()
    adresse = entryAdresse.get()
    telephone = entryTel.get()
    statut = entryStatut.get()

    # connection dataBase
    conn = sqlite3.connect('hopital.db')
    cur = conn.cursor()
    req1 = "CREATE TABLE IF NOT EXISTS patient (id INTEGER PRIMARY KEY AUTOINCREMENT," \
           "matricule TEXT NOT NULL," \
           "nom TEXT NOT NULL," \
           "prenom TEXT NOT NULL," \
           "age INTEGER NOT NULL," \
           "adresse TEXT NOT NULL," \
           "telephone TEXT NOT NULL," \
           "statut TEXT NOT NULL)"
    cur.execute(req1)

    req2 = "INSERT INTO patient (matricule, nom, prenom, age, adresse, telephone, statut) values (? ,?, ?, ?, ?, ?, ?)"
    cur.execute(req2, (matricule, nom, prenom, age, adresse, telephone, statut))
    conn.commit()
    conn.close()
    messagebox.showinfo("Patient ajouté")
    # Afficher
    conn = sqlite3.connect('hopital.db')
    cur = conn.cursor()
    maj = cur.execute("select * from patient order by id desc")
    maj = list(maj)
    tree.insert('', END, values=maj[0])
    conn.close()

def modifier():
    matricule = entryMatricule.get()
    nom = entryNom.get()
    prenom = entryPrenom.get()
    age = entryAge.get()
    adresse = entryAdresse.get()
    telephone = entryTel.get()
    statut = entryStatut.get()

    # connection dataBase
    conn = sqlite3.connect('hopital.db')
    cur = conn.cursor()
    req1 = "UPDATE patient set matricule=?, nom=?, prenom=?, age=?, adresse=?, telephone=?, statut=? WHERE id=?"
    cur.execute(req1, (id, matricule, nom, prenom, age, adresse, telephone, statut))
    conn.commit()
    conn.close()
    messagebox.showinfo("Patient modifié")
    #Afficher
    conn = sqlite3.connect('hopital.db')
    cur = conn.cursor()
    maj = cur.execute("select * from patient order by id desc")
    maj = list(maj)
    tree.insert('', END, values= maj[0])
    conn.close()

def supprimer() :
    codeSelectionner = tree.item(tree.selection())['values'][0]
    conn = sqlite3.connect('hopital.db')
    cur = conn.cursor()
    delete = cur.execute("delete from patient where id= {}".format(codeSelectionner))
    conn.commit()
    tree.delete(tree.selection())


# créer la fenètre
window = Tk()

# Personnaliser
window.title("Gestion des patients")
window.geometry('1300x670')
window.config(bg= 'grey')


# Titre generale
grow_Title = Label(window, bd= 13, relief= RIDGE, text='GESTION DES PATIENTS CHEZ BODY-HOPITAL4', font=('Robboto', 25), bg= 'darkblue', fg= 'white' )
grow_Title.place(x= 0, y= 0, width= 1300, height= 75)

underTitle = Label(window, text= 'Liste des patients', bg= 'darkblue', font=('Robboto', 15), fg= 'white')
underTitle.place(x= 480, y= 100, width= 800, height= 35)

# Ajouter patients

    # Texte matricule
matriculePat = Label(window, text=' Matricule Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
matriculePat.place(x= 0, y= 170, width= 200)
entryMatricule = Entry(window, bg= 'yellow')
entryMatricule.place(x= 200, y= 170, width= 160, height= 30)

    # Texte nom
nomPat = Label(window, text='Nom Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
nomPat.place(x= 0, y= 220, width= 200)
entryNom = Entry(window, bg= 'yellow')
entryNom.place(x= 200, y= 220, width= 160, height= 30)

    # Texte prenom
prenomPat = Label(window, text= 'Prenom Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
prenomPat.place(x= 0, y= 270, width= 200)
entryPrenom = Entry(window, bg= 'yellow')
entryPrenom.place(x= 200, y= 270, width= 160, height= 30)

    # Texte age
agePat = Label(window, text= 'Age Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
agePat.place(x= 0, y= 320, width= 200)
entryAge = Entry(window, bg= 'yellow')
entryAge.place(x= 200, y= 320, width= 160, height= 30)

    # Texte adresse
adressePat = Label(window, text= 'Adresse Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
adressePat.place(x= 0, y= 370, width= 200)
entryAdresse = Entry(window, bg= 'yellow')
entryAdresse.place(x= 200, y= 370, width= 160, height= 30)

    # Texte téléphone
telPat = Label(window, text= 'Téléphone Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
telPat.place(x= 0, y= 420, width= 200)
entryTel = Entry(window, bg= 'yellow')
entryTel.place(x= 200, y= 420, width= 160, height= 30)

    # Texte satut
statutPat = Label(window, text= 'Statut Patient', font=('Berlin', 15), bg= 'black', fg= 'white')
statutPat.place(x= 0, y= 470, width= 200)
entryStatut = Entry(window, bg= 'yellow')
entryStatut.place(x= 200, y= 470, width= 160, height= 30)

# Ajouter des boutons

    # Enregistrer
btnRegister = Button(window, text= 'Enregistrer', font=('Arial', 15), bg= 'darkblue', fg= 'yellow', command= register)
btnRegister.place(x= 25, y= 520, width= 160, height= 38)

    # Modifier
btnModifier = Button(window, text= 'Modifier', font=('Arial', 15), bg= 'darkblue', fg= 'yellow', command= modifier)
btnModifier.place(x= 220, y= 520, width= 160, height= 38)

    # Supprimer
btnDelete = Button(window, text= 'Supprimer', font=('Arial', 15), bg= 'darkblue', fg= 'yellow', command= supprimer)
btnDelete.place(x= 120, y= 570, width= 160, height= 38)

    # Quitter
btnExit = Button(window, text= 'Quitter', font=('Arial', 15), bg= 'darkblue', fg= 'yellow', command= window.quit)
btnExit.place(x= 800, y= 600, width= 160, height= 38)


tree = ttk.Treeview(window, columns= (1, 2, 3, 4, 5, 6, 7, 8), heigh= 5, show= 'headings')
tree.place(x= 470, y= 150, width= 820, height= 410)

# entete et dimension
tree.heading(1, text= 'ID')
tree.heading(2, text= 'Matricule')
tree.heading(3, text= 'Nom')
tree.heading(4, text= 'Prenom')
tree.heading(5, text= 'Age')
tree.heading(6, text= 'Adresse')
tree.heading(7, text= 'Telephone')
tree.heading(8, text= 'Statut')

tree.column(1, width= 25)
tree.column(2, width= 95)
tree.column(3, width= 100)
tree.column(4, width= 100)
tree.column(5, width= 25)
tree.column(6, width= 100)
tree.column(7, width= 90)
tree.column(8, width= 100)

conn = sqlite3.connect('hopital.db')
cur = conn.cursor()
req = "select * from patient"
cur.execute(req)

# Afficher les données dans treeview
select = cur.execute(req)
for row in select :
    tree.insert('', END, values= row)
conn.close()

# Affcher la fenètre
window.mainloop()
